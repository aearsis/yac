#include "yac.h"
#include <stdio.h>
#include <time.h>

#ifndef UTIL_H
#define UTIL_H

void util_file_editor(char * path);
int util_file_mtime(char * path);
void util_vim_powers(FILE *file);
int util_parse_date(char * input, struct tm * date);

#endif
