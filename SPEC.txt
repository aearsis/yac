Konzolový kalendář - yac (yet another calendar)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- implementace jednoduchého plánovacího kalendáře
- ovládací rozhraní konzolové
	- "git-like"
	- editace událostí v textovém editoru
- události jsou intervaly času s názvem, rozšířenou poznámkou
  + uživatelem definovaným typem události (typy jsou rovnocenné)
  + flagy (future-compatibility)
- konfigurace - ~/.yacrc
- snadná integrace do grafického prostředí - co se bude hodit :)
- jazyk C, předpokládám využití knihoven Cairo a libssh

- povinné featury
	- decentralizovaná synchronizace po síti (možná ssh?) s řešením
	  časových konfliktů
	- grafický výstup - (pdf|svg|tex)

- povinně volitelné featury
	- výstupní moduly - text, TeX, pdf, svg, iCal, html, ...
	- periodické události
	- grafický výstup pro týden, měsíc, ...
	- cronové upozorňovátko
		- tohle by bylo pěkné na serveru, kdyby posílalo maily
		- eventuelně flagem zařídit vynucení pozornosti majitele
	- vyhledávání v událostech i minulých (index)
