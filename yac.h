#ifndef YAC_H
#define YAC_H

#include <ucw/lib.h>
#include <errno.h>
#include <sys/types.h>
#include <limits.h>

typedef struct {
	char *cf_path, *path, *cal,
		 **actions, *action, *title,
		 *type, *start, *end, *remote;
	int run_editor, prefix_len;
	size_t path_len, cal_len;
} yac_config;

static yac_config config = {
	.cf_path = "~/.yac",
	.path = NULL,
	.cal = "default",
	.action = "help",
	.type = "event",
	.remote = "origin",
	.prefix_len = 3,
	.run_editor = 1,
};

typedef char path_t [PATH_MAX];

#endif
