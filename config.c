#include "yac.h"
#include <ucw/conf.h>
#include <ucw/opt.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <wordexp.h>

static struct cf_section yac_cf = {
	CF_ITEMS {
		CF_STRING("Path", &config.cf_path),
		CF_STRING("Calendar", &config.cal),
		CF_STRING("Type", &config.type),
		CF_INT("PrefixLen", &config.prefix_len),
		CF_END
	}
};

static struct opt_section options = {
	OPT_ITEMS {
		OPT_HELP("Yet Another Calendar"),
		OPT_HELP("===================="),
		OPT_HELP("Usage: yac [-options] <action>"),
		OPT_HELP(""),
		OPT_HELP("Actions:"),
		OPT_HELP("create [type] [title]\tOpens editor for new event"),
		OPT_HELP("edit <id>\tOpens editor for new event"),
		OPT_HELP("future\tShows following events"),
		OPT_HELP("sync [repo]\tSynchronises with repository"),
		OPT_HELP("set-url url [repo]\tSet's repository url"),
		OPT_HELP("reindex\tRebuilds index"),
		OPT_HELP("show\tLists all events"),
		OPT_HELP(""),
		OPT_HELP("Options:"),
		OPT_HELP_OPTION,
		OPT_CONF_OPTIONS,
		OPT_STRING('l', "title", config.title, 0, "\"\"\ttitle for new event, search etc."),
		OPT_STRING('t', "type", config.type, 0, "event\ttype for new event, search etc."),
		OPT_STRING('s', "start", config.start, 0, "\"\"\tstart date for new event, search etc."),
		OPT_STRING('e', "end", config.end, 0, "\"\"\tend date for new event, search etc."),
		OPT_BOOL('n', "noedit", config.run_editor, OPT_NEGATIVE, "\tDo not open editor"),
		OPT_STRING_MULTIPLE(OPT_POSITIONAL_TAIL, NULL, config.actions, 0, "\tAction to be taken"),
		OPT_END
	}
};

static void CONSTRUCTOR yac_config_init(void) {
	cf_declare_section("Yac", &yac_cf, 0);
}

void load_config(char **argv)
{
	struct passwd *pw = getpwuid(getuid());

	path_t path;
	strncpy(path, pw->pw_dir, PATH_MAX);
	strcat(path, "/.yacrc");
	cf_def_file = path;

	GARY_INIT(config.actions, 1);
	opt_parse(&options, argv+1);

	if (GARY_SIZE(config.actions) > 1) {
		config.action = config.actions[1];
	}

	wordexp_t p;
	switch (wordexp(config.cf_path, &p, 0)) {
		case WRDE_BADCHAR:
			printf("Configuration error: Bad character in Path\n");
			exit(1);
		case 0:
			break;
		default:
			printf("Expanding path: Unknown error\n");
			exit(2);
	}
	if (p.we_wordc < 1) {
		printf("Configuration error: No path specified\n");
		exit(1);
	}
	if (NULL == (config.path = realpath(p.we_wordv[0], NULL))) {
		perror("Realpath");
		exit(errno);
	}
	wordfree(&p);

	config.path_len = strlen(config.path);
	config.cal_len = strlen(config.cal);
}

void config_action_help()
{
	opt_help(&options);
	exit(0);
}
