#include "yac.h"
#include "util.h"
#include "event.h"
#include "cal.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>

/**
 * Dills dest with file path to event
 */
size_t event_path(char * dest, event_id id)
{
	size_t len = cal_path(dest);
	stpncpy(dest + len, "/events/", PATH_MAX - len); len += 8;
	stpncpy(dest + len, id, PATH_MAX - len);
	dest[len + EVENT_ID_LEN] = 0;
	return len + EVENT_ID_LEN;
}

/**
 * Returns true iff the event exists
 */
bool event_exists(event_id id)
{
	struct stat buf;
	path_t path;
	event_path(path, id);
	if (0 == stat(path, &buf))
		return true;
	return false;
}

/**
 * Prints date to file in defined format
 */
void event_print_date(FILE *stream, const char * format, time_t *date)
{
	if (*date == 0) {
		fprintf(stream, format, "");
		return;
	}
	struct tm * tmp;
	char buf[256];

	tmp = localtime(date);
	if (0 == strftime(buf, 256, DATE_FORMAT, tmp)) {
		perror("Formatting date"); 
		exit(errno);
	}
	fprintf(stream, format, buf);
}

/**
 * Wraps call to util_parse_date
 */
int event_parse_date(char * input, time_t *output)
{
	while (isspace(*input) && *input) input++;
	if (*input == 0) {
		*output = 0;
		return 0;
	}

	struct tm tmp;
	if (!util_parse_date(input, &tmp)) {
		return 0;
	}

	*output = mktime(&tmp);
	return 1;
}

/**
 * Reads ifle to end, pushing everything to description.
 */
void event_read_description(FILE *ev, char **description) {
	char *desc, *buf = NULL;
	size_t size = 4096, len = 0, bufsize = 0;
	ssize_t n;
	desc = xmalloc(size);
	desc[0] = '\0';

	while (1) {
		n = getline(&buf, &bufsize, ev);
		if (0 >= n)
			break;
		if ('#' == buf[0])
			continue;

		while (len + n >= size) {
			size *= 2;
			desc = xrealloc(desc, size);
		}
		strcpy(desc + len, buf);
		len += n;
	}

	*description = desc;
	free(buf);
}

/**
 * Reads saved event. Will break if the file is invalid.
 */
void event_read_file(FILE *ev, event *e)
{
	char buf [4096];
	char *line, *header, *value, *v;
	while (1) {
		if (NULL == (line = fgets(buf, 4096, ev)))
			break;
		while (*line && isspace(*line)) line++;
		if (0 == *line)
			break;
		if ('#' == line[0])
			continue;

		sscanf(line, "%m[^:]:%m[^\n]", &header, &value);
		v = value;
		while (*v && isspace(*v)) v++;
		
		if (0 == strcasecmp(header, "Type")) {
			e->type = xstrdup(v);
		} else if (0 == strcasecmp(header, "Title")) {
			e->title = xstrdup(v);
		} else if (0 == strcasecmp(header, "Start")) {
			event_parse_date(v, &e->start);
		} else if (0 == strcasecmp(header, "End")) {
			event_parse_date(v, &e->end);
		} else {
			event_info *evi = malloc(sizeof(event_info));
			evi->key = xstrdup(header);
			evi->value = xstrdup(value);
			evi->next = e->info;
			e->info = evi;
		}

		free(header);
		free(value);
		header = value = NULL;
	}
	
	event_read_description(ev, &e->description);
}

/**
 * Loads all the information for event. e->id must be set.
 */
void event_read(event *e) {
	cal_init();

	path_t path;
	FILE * ev;

	event_path(path, e->id);
	if (!(ev = fopen(path,"r"))) {
		perror("Opening event");
		exit(errno);
	}

	event_read_file(ev, e);

	fclose(ev);
}

/**
 * Magicaly reads potentially invalid file. Tries to guess date format.
 */
#define F_TITLE	0x1
#define F_START	0x2
#define F_END	0x4
int event_try_parse(char * path)
{
	int success = 1;
	FILE *file, *ev = tmpfile();
	if (!(file = fopen(path, "r"))) {
		perror("Opening edited file");
		exit(errno);
	}
	util_copy_file(file, ev);
	rewind(ev);
	fclose(file);
	if (!(file = fopen(path, "w"))) {
		perror("Opening edited file");
		exit(errno);
	}
	fprintf(file, "# There were some errors. Please correct them before saving again.\n# To cancel, close this editor without saving the file\n");
	
	char buf [4096];
	char *line, *header, *value, *v, *newdate;
	int features = 0;
	time_t datetime;
	while (1) {
		if (NULL == (line = fgets(buf, 4096, ev)))
			break;
		while (*line && isspace(*line)) line++;
		if (0 == *line)
			break;
		if ('#' == line[0])
			continue;

		sscanf(line, "%m[^:]:%m[^\n]", &header, &value);
		v = value;
		while (isspace(*v) && *v) v++;
		if (0 == strcasecmp(header, "Title")) {
			if ((features & F_TITLE) == 0) {
				features |= F_TITLE;
			} else {
				fprintf(file, "# Superfluous title\n#");
				success = 0;
			}
		} else if (0 == strcasecmp(header, "Start")) {
			if ((features & F_START) == 0) {
				features |= F_START;
				if (*v) {
					if (!event_parse_date(v, &datetime)) {
						fprintf(file, "# Unknown date format, please consult the manual\n");
						success = 0;
					} else {
						newdate = xmalloc(256);
						strftime(newdate, 256, DATE_FORMAT, localtime(&datetime));
						if (strcmp(newdate, v) != 0) {
							free(value);
							v = value = newdate;
							fprintf(file, "# Parsed date as following. Please check.\n");
							success = 0;
						} else free(newdate);
					}
				}
			} else {
				fprintf(file, "# Superfluous start date\n#");
				success = 0;
			}
		} else if (0 == strcasecmp(header, "End")) {
			if ((features & F_END) == 0) {
				features |= F_END;
				if (*v) {
					if (!event_parse_date(v, &datetime)) {
						fprintf(file, "# Unknown date format, please consult the manual\n");
						success = 0;
					} else {
						newdate = xmalloc(256);
						strftime(newdate, 256, DATE_FORMAT, localtime(&datetime));
						if (strcmp(newdate, v) != 0) {
							free(value);
							v = value = newdate;
							fprintf(file, "# Parsed date as following. Please check.\n");
							success = 0;
						} else free(newdate);
					}
				} else if (datetime != 0) {
					newdate = xmalloc(256);
					datetime += 3600;
					strftime(newdate, 256, DATE_FORMAT, localtime(&datetime));
					free(value);
					v = value = newdate;
					fprintf(file, "# End date guessed as following. Please check.\n");
					success = 0;
				}
			} else {
				fprintf(file, "# Superfluous end date\n#");
				success = 0;
			}
		}
		if (!*v) {
			fprintf(file, "# Empty header - delete or fill it\n");
			success = 0;
		}
		
		fprintf(file, "%s: %s\n", header, v);
		free(header);
		free(value);
		header = value = line = NULL;
	}

	if ((features & F_TITLE) == 0) {
		fprintf(file, "# Missing title\nTitle: \n");
		success = 0;
	}
	if ((features & F_START) == 0) {
		fprintf(file, "# Missing start date\nStart: \n");
		success = 0;
	}
	if ((features & F_END) == 0) {
		fprintf(file, "# Missing end date\nEnd: \n");
		success = 0;
	}

	fprintf(file, "\n");
	event_read_description(ev, &line);
	fprintf(file, "%s", line);
	free(line);

	util_vim_powers(file);
	fclose(file);
	fclose(ev);
	printf("Check syntax: %s\n", success ? "OK" : "FAILED");
	return success;
}

/**
 * Writes event information to file.
 */
void event_write_file(FILE *ev, event *e) {
	fprintf(ev, "Title: %s\n", (e->title == NULL) ? "" : e->title);
	fprintf(ev, "Type: %s\n", e->type);
	event_print_date(ev, "Start: %s\n", &e->start);
	event_print_date(ev, "End: %s\n", &e->end);
	event_info *i = e->info;
	while (i) {
		fprintf(ev, "%s: %s\n", i->key, i->value);
		i = i->next;
	}
	fprintf(ev, "\n%s", (e->description == NULL) ? "" : e->description);
}

/**
 * Inserts event to index. Although reading and removing from "index" events is
 * possible, inserting to it will effectively truncate that critical file.
 */
void event_insert_to_index(event *e, char * index)
{
	printf("Inserting \"%s\" to index %s\n", e->title, index);
	path_t p, cpath, epath;
	cal_path_subdir(cpath, index);

	snprintf(p, PATH_MAX, "%s/%.*s", cpath, EVENT_ID_LEN, e->id);
	event_path(epath, e->id);

	char *dirc, *dname;
	dirc = xstrdup(p);
	dname = dirname(dirc);
	util_recursive_mkdir(dname);
	free(dirc);

	int fd = open(p, O_CREAT|O_TRUNC|O_RDONLY, 00440); // The file should remain empty
	if (-1 == fd && errno != EEXIST) {
		perror("Creating index");
		exit(1);
	}
	if (-1 != fd)
		close(fd);
}

/**
 * Removes an event from index. Can be used to delete event - use index "events".
 */
void event_remove_from_index(event *e, char * index)
{
	if (e->title)
		printf("Removing \"%s\" from index %s\n", e->title, index);
	path_t p, cpath;
	cal_path_subdir(cpath, index);

	snprintf(p, PATH_MAX, "%s/%.*s", cpath, EVENT_ID_LEN, e->id);

	char *dirc, *dname;
	dirc = xstrdup(p);
	dname = dirname(dirc);
	util_recursive_mkdir(dname);
	free(dirc);

	unlink(p);
}

/**
 * Inserts event to indexes where it belongs. Does not do the opposite, indexes
 * are just an advice.
 */
void event_update_index(event *e)
{
	struct tm day;
	localtime_r(&e->start, &day);
	path_t index_name;

	day.tm_hour = day.tm_min = day.tm_sec = 0;

	if (e->end > time(NULL))
		event_insert_to_index(e, "future");
	
	while (mktime(&day) <= e->end) {
		strftime(index_name, PATH_MAX, "date/%Y/%m/%d", &day);
		event_insert_to_index(e, index_name);
		day.tm_mday++;
	}
}

/**
 * Fills list with events from index.
 */
void event_read_index(event_list *list, char * index)
{
	path_t calp;
	cal_path_subdir(calp, index);
	DIR *dirp = opendir(calp);
	struct dirent *ent;
	while (NULL != (ent = readdir(dirp))) {
		if (ent->d_type != DT_REG)
			continue;
		event_list_node *en = event_list_new(list);
		event_init(&en->e, ent->d_name);
	}
	closedir(dirp);
}

/**
 * Saves an event, updates indexes.
 */
void event_write(event *e) {
	cal_init();

	path_t path;
	FILE * ev;

	event_path(path, e->id);
	if (!(ev = fopen(path, "w+"))) {
		perror("Writing event");
		exit(errno);
	}

	event_write_file(ev, e);
	event_update_index(e);

	if (fclose(ev) != 0) {
		perror("Writing event");
		exit(errno);
	}
}

/**
 * Let the user edit file, until it's properly formatted or not modified at
 * all.
 */
int event_edit_loop(char * path)
{
	do {
		int mtime = util_file_mtime(path) - 86400;
		util_file_set_mtime(path, mtime);
		util_file_editor(path);
		if (mtime == util_file_mtime(path))
			return 0;
	} while (!event_try_parse(path));
	return 1;
}

/**
 * Initializes event structure
 */
void event_init(event *e, event_id id) {
	if (id != NULL)
		strncpy(e->id, id, EVENT_ID_LEN);
	e->type = e->title = e->description = NULL;
	e->start = e->end = 0;
	e->info = NULL;
}

/**
 * Initializes event structure and generates an unique file name for it
 */
void event_init_new(event * e)
{
	static char * charmap = "1234567890qwertyuiopasdfghjklzxcvbnm";
	do {
		for (int i = 0; i < EVENT_ID_LEN; i++) {
			e->id[i] = charmap[rand() % 36];
		}
	} while (event_exists(e->id));
	event_init(e, NULL);
	e->type = xstrdup(config.type);
	if (config.title != NULL)
		e->title = xstrdup(config.title);
	if (config.start != NULL)
		event_parse_date(config.start, &e->start);
	if (config.end != NULL)
		event_parse_date(config.end, &e->end);
}

/**
 * Frees the memory allocated by event_read
 */
void event_free_contents(event *e) {
	while (e->info != NULL) {
		event_info *n = e->info;
		e->info = n->next;
		free(n);
	}
	free(e->description);
	free(e->title);
	free(e->type);
}

/**
 * Frees the insides and the event structure itself
 */
void event_free(event *e) {
	event_free_contents(e);
	free(e);
}

/**
 * Dumps all the information about en event. For debugging purposes.
 */
void event_dump(event *e)
{
	printf("Event (%.*s)\n=======\n", EVENT_ID_LEN, e->id);
	printf("Title: %s\n", e->title);
	printf("Type: %s\n", e->type);
	event_print_date(stdout, "Start: %s\n", &e->start);
	event_print_date(stdout, "End: %s\n", &e->end);
	event_info *i = e->info;
	while (i) {
		printf("*%s: %s\n", i->key, i->value);
		i = i->next;
	}
	printf("Description: \"%s\"\n=======\n", e->description == NULL ? "" : e->description);
}

/**
 * Prints nicely formatted line.
 */
void event_print_oneline(event *e)
{
	event_print_date(stdout, "%s - ", &e->start);
	event_print_date(stdout, "%s: ", &e->end);
	printf("%s (%s %.*s)\n", e->title, e->type, config.prefix_len, e->id);
	event_info *i = e->info;
	while (i) {
		printf("\t* %s: %s\n", i->key, i->value);
		i = i->next;
	}
}

/**
 * Edits an event.
 */
int event_edit(event * e) {
	int return_code = 0;
	char * tempfile = tempnam(NULL, "yac-");
	if (tempfile == NULL) {
		   perror("Temporary file");
		   exit(errno);
	}
	FILE *temp = fopen(tempfile, "w+");

	fprintf(temp, "# Lines starting with # will be ignored\n# To cancel the operation, close this editor without saving\n# Please write dates in format dd.mm.yyyy hh:mm\n# Everything after first empty line will be treated as a long description\n");
	event_write_file(temp, e);
	util_vim_powers(temp);
	fclose(temp);

	if ((return_code = event_edit_loop(tempfile))) {
		temp = fopen(tempfile, "r");
		event_read_file(temp, e);
		fclose(temp);
	}

	if (-1 == unlink(tempfile)) {
		perror("Deleting temp file");
	}

	free(tempfile);
	return return_code;
}

/**
 * Action to create new event and let the user edit it. If the event loop isn't
 * finished, the file for that event is not created.
 */
void event_action_create()
{
	event e;
	event_init_new(&e);

	if (event_edit(&e)) {
		event_write(&e);
		
		cal_commit("Created event");
	} else {
		printf("Operation was cancelled.\n");
	}
}

/**
 * Edits an event specified by config.title
 */
void event_action_edit()
{
	if (!config.title) {
		printf("No id specified\n");
		return;
	}
	int len = strlen(config.title);
	if (len < config.prefix_len || len > EVENT_ID_LEN) {
		printf("Wrong length\n");
		return;
	}

	event_list events;
	event_list_init(&events);
	event_read_index(&events, "events");

	event *event;
	event_list_walk(&events, event) {
		if (!strncmp(event->id, config.title, len)) {
			event_read(event);
			if (event_edit(event)) {
				event_write(event);
				event_free_contents(event);

				cal_commit("Edited event");
				break;
			}
			event_free_contents(event);
		}
	}
}

/**
 * Deletes an event specified by config.title
 */
void event_action_delete()
{
	if (!config.title) {
		printf("No id specified\n");
		return;
	}
	int len = strlen(config.title);
	if (len < config.prefix_len || len > EVENT_ID_LEN) {
		printf("Wrong length\n");
		return;
	}

	event_list events;
	event_list_init(&events);
	event_read_index(&events, "events");

	event *event;
	event_list_walk(&events, event) {
		if (!strncmp(event->id, config.title, len)) {
			event_read(event);
			event_remove_from_index(event, "events");
			printf("Event %s deleted.\n", event->title);
			cal_commit("Deleted event");
			event_free_contents(event);
			break;
		}
	}
}

/**
 * Dumps all created events.
 */
void event_action_show()
{
	event_list events;
	event_list_init(&events);
	event_read_index(&events, "events");

	event *event;
	event_list_walk(&events, event) {
		if (!event_exists(event->id)) {
			event_remove_from_index(event, "events");
			continue;
		}
		event_read(event);
		event_dump(event);
		event_free_contents(event);
	}
}

/**
 * Prints events in the future, sorted by date.
 */
void event_action_future()
{
	event_list events;
	event_list_init(&events);
	event_read_index(&events, "future");

	event *event;
	time_t now = time(NULL);
	event_list_walk(&events, event) {
		if (!event_exists(event->id)) {
			event_remove_from_index(event, "future");
			event_list_remove(&events, _event_list_walker);
		}
		event_read(event);
		if (event->end < now) {
			event_remove_from_index(event, "future");
			event_free_contents(event);
			continue;
		}
	}

	event_list_sort_by_date(&events);

	event_list_walk(&events, event) {
		event_print_oneline(event);
		event_free_contents(event);
	}
}

/**
 * Inserts all events to their indexes. Shouldn't be necessery, as the events
 * are added to indexes automatically after saving.
 */
void event_action_reindex()
{
	event_list events;
	event_list_init(&events);
	event_read_index(&events, "events");

	event *event;
	event_list_walk(&events, event) {
		event_read(event);
		event_update_index(event);
		event_free_contents(event);
	}
}
