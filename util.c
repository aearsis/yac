#include "yac.h"
#include "util.h"
#include "cal.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <signal.h>
#include <libgen.h>

#define DF_YEAR 1
#define DF_MONTH 2
#define DF_WEEK_DAY 4
#define DF_END { .format = NULL, .flags = 0 }
#define DF(str, f) { .format = str, .flags = f }
struct date_format {
	char * format;
	int flags;
};

/**
 * Recognized date formats
 */
static struct date_format date_formats [] = {
	DF("%d.%m.%y %H:%M:%S", DF_YEAR | DF_MONTH),
	DF("%d.%m.%Y %H:%M:%S", DF_YEAR | DF_MONTH),
	DF("%d.%m.%y %H:%M", DF_YEAR | DF_MONTH),
	DF("%d.%m.%Y %H:%M", DF_YEAR | DF_MONTH),
	DF("%d.%m. %H:%M:%S", DF_MONTH ),
	DF("%d.%m. %H:%M", DF_MONTH ),
	DF("%d. %H:%M", 0),
	DF("%H:%M:%S", 0),
	DF("%H:%M", 0),
	DF("%d.%m.%y", DF_YEAR | DF_MONTH),
	DF("%d.%m.%Y", DF_YEAR | DF_MONTH),
	DF("%d.%m.", DF_MONTH),
	DF("%a %H:%M:%S", DF_WEEK_DAY),
	DF("%a %H:%M", DF_WEEK_DAY),
	DF("%A %H:%M:%S", DF_WEEK_DAY),
	DF("%A %H:%M", DF_WEEK_DAY),
	DF("%d.", 0),
	DF_END
};

/**
 * Fires an $EDITOR for the specified file, waits until closed.
 * Hard-coded skipping first 4 comment lines
 */
void util_file_editor(char * path)
{
	char * editor = getenv("EDITOR");
	if (editor == NULL) editor = "vi";
	
	pid_t pid, wpid;
	int status;
	if ((pid = fork())) {
		if (pid == -1) {
			perror("Fork");
			exit(errno);
		}
		do {
			wpid = waitpid(pid, &status, 0);
			if (wpid == -1) {
				perror("Waitpid");
				exit(EXIT_FAILURE);
			}
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
	} else {
		execlp(editor, editor, "+/^[^#:]\\+:", "-f", path, NULL);

		perror("Executing editor");
		exit(errno);
	}
}

/**
 * Byte-copies files.
 */
void util_copy_file(FILE * from, FILE * to) {
	rewind(from);

	char buff [4096];
	size_t n;

	while ((n = fread(buff, 1, 4096, from))) {
		fwrite(buff, 1, n, to);
	}
}

/**
 * Gets file mtime
 */
int util_file_mtime(char * path)
{
	struct stat t;
	if (-1 == stat(path, &t)) {
		perror("Stat");
		exit(errno);
	}

	return t.st_mtime;
}

/**
 * Sets file mtime
 */
int util_file_set_mtime(char *path, int mtime)
{
	struct timeval times [2] = {
		{ .tv_sec = mtime },
		{ .tv_sec = mtime }
	};

	return utimes(path, times);
}

/**
 * Unleash the powers!
 */
void util_vim_powers(FILE *file)
{
	fprintf(file, "# vim:syntax=yac\n");
}

/**
 * Parses date given by guessing format from date_formats.
 */
int util_parse_date(char * input, struct tm * date)
{
	struct tm now;
	time_t now_t, date_t;
	char * stop;
	time(&now_t);
	localtime_r(&now_t, &now);
	now.tm_sec = 0;
	now.tm_min = 0;

	for (int i = 0; date_formats[i].format; i++) {
		memcpy(date, &now, sizeof(struct tm));
		stop = strptime(input, date_formats[i].format, date);
		if (stop != NULL && '\0' == *stop) {
			if ((date_formats[i].flags & DF_WEEK_DAY) == DF_WEEK_DAY)
			// If week day was specified, alter the date accordingly for mktime
				date->tm_mday += ((7 + date->tm_wday - now.tm_wday) % 7);
			// We typicallu want to create events in the future
			date_t = mktime(date);
			if ((date_formats[i].flags & DF_MONTH) == 0 && date_t + 605000 < now_t) // +-1 week
				date->tm_mon++;
			if ((date_formats[i].flags & (DF_MONTH | DF_YEAR)) == DF_MONTH && date_t + 605000 < now_t)
				date->tm_year++;

			return i + 1;
		}
	}
	return 0;
}

void util_recursive_mkdir(char *path)
{
	if (-1 == mkdir(path, 0750)) {
		if (errno == ENOENT) {
			char *dirc, *dname;
			dirc = xstrdup(path);
			dname = dirname(dirc);
			util_recursive_mkdir(dname);
			free(dirc);
			errno = 0;
			if (0 == mkdir(path, 0750))
				return;
		}
		if (errno == EEXIST) {
			errno = 0;
		} else {
			perror("Making directory");
			exit(errno);
		}
	}
}

int util_system(char * command) {
	return system(command);
}

int util_git_init() {
	return util_system("[ -d .git ] || git init");
}

int util_git_commit(char * message) {
	setenv("message", message, 1);
	return util_system("git add * && git commit -am \"$message\"");
}

int util_git_sync() {
	setenv("remote", config.remote, 1);
	return util_system("git pull \"$remote\" && git push \"$remote\" \"master\"");
}

int util_git_set(char *url) {
	setenv("remote", config.remote, 1);
	setenv("url", url, 1);
	return util_system("(git remote add \"$remote\" \"$url\" || git remote set-url \"$remote\" \"$url\") 2> /dev/null");
}

void util_git_recall(int argc, char ** argv) {
	cal_git_init();
	argv[0] = "/usr/bin/git";
	execv("/usr/bin/git", argv);
	perror("Executing git");
}
