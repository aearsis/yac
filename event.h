#ifndef EVENT_H
#define EVENT_H

#define EVENT_ID_LEN 32
#define DATE_FORMAT "%d.%m.%Y %H:%M"

typedef char event_id [EVENT_ID_LEN];

typedef struct s_event_info {
	char *key, *value;
	struct s_event_info *next;
} event_info;

typedef struct s_event {
	event_id id;
	time_t start, end;
	char * description;
	char * title;
	char * type;
	event_info *info;

	int read;
} event;

void event_init(event *e, event_id id);
#endif
