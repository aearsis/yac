CC=gcc
#CC=clang --analyze
CFLAGS=-std=c99 -g -I/usr/local/include -L/usr/local/lib -lucw-6.0 -g -O2

all: yac doc

yac: yac.c yac.h config.c event.c cal.c util.c
	${CC} ${CFLAGS} $< -o $@

tags:
	ctags -Ra

doc.pdf: doc.tex
	pdfcsplain doc.tex

doc: doc.pdf


clean:
	rm yac

.PHONY: all clear ctags doc
