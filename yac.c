#include "yac.h"
#include "util.c"
#include "config.c"
#include "cal.c"
#include "event.c"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int action_len;

int route_action(const char * action) {
	int alen = strlen(action);
	return 0 == strncmp(config.action, action, action_len > alen ? alen : action_len);
}

int main(int argc, char **argv)
{
	load_config(argv);
	cal_init();
	srand((unsigned int) time(NULL));

	int pos_args = GARY_SIZE(config.actions);

	action_len = strlen(config.action);
	if (route_action("create") || route_action("new")) {
		if (pos_args > 2)
			config.type = config.actions[2];
		if (pos_args > 3)
			config.title = config.actions[3];
		event_action_create();

	} else if (route_action("edit")) {
		if (pos_args <= 2) {
			printf("Please specify id.\n");
			return 1;
		}
		config.title = config.actions[2];
		event_action_edit();

	} else if (route_action("delete")) {
		if (pos_args <= 2) {
			printf("Please specify id.\n");
			return 1;
		}
		config.title = config.actions[2];
		event_action_delete();

	} else if (route_action("show")) {
		if (pos_args > 2)
			config.type = config.actions[2];
		event_action_show();

	} else if (route_action("future")) {
		event_action_future();

	} else if (route_action("reindex")) {
		event_action_reindex();

	} else if (route_action("sync")) {
		if (pos_args > 2)
			config.remote = config.actions[2];
		cal_action_sync();

	} else if (route_action("set-url")) {
		if (pos_args > 3)
			config.remote = config.actions[3];
		if (pos_args < 2) {
			printf("Please provide the url.");
			return 1;
		}
		cal_action_seturl(config.actions[2]);

	} else if (route_action("remote")) {
		util_git_recall(argc, argv);

	} else {
		config_action_help();
	}
}
