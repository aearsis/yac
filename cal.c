#include "cal.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ucw/mempool.h>
#include <unistd.h>

/**
 * Fills dest with path to calendar
 * @return length
 */
size_t cal_path(char * dest)
{
    path_t path;
    strncpy(path, config.path, PATH_MAX);
    path[config.path_len] = '/';
    strncpy(path + config.path_len + 1, config.cal, PATH_MAX - config.path_len - 1);
    realpath(path, dest);
    return strlen(dest);
}

/**
 * Fills dest with path to subdirectory
 * @return length
 */
size_t cal_path_subdir(char * dest, char * subdir)
{
	size_t len = cal_path(dest);
	dest[len] = '/';
	strncpy(dest + len + 1, subdir, PATH_MAX - len - 1);
	return strlen(dest);
}

/**
 * Creates subdirectory of calendar
 */
void cal_mksubdir(char * name)
{
    path_t path, real;
    size_t len = cal_path(path);
    if (name != NULL) {
        path[len] = '/';
        strncpy(path + len + 1, name, PATH_MAX - len - 1);
    }
    realpath(path, real);
    if (-1 == mkdir(real, 0770)) {
        if (errno == EEXIST) {
            errno = 0;
        } else {
            perror("Creating calendar");
            exit(errno);
        }
    } else if (name == NULL) {
        printf("Created directory for calendar '%s'.\n", config.cal);
    }
}

/**
 * Creates all necessary directories
 */
void cal_init()
{
    cal_mksubdir("..");
    cal_mksubdir(NULL);
    cal_mksubdir("events");
    cal_mksubdir("future"); 
}

/**
 * Initializes git repository
 */
void cal_git_init()
{
	path_t cal;
	cal_path(cal);
	chdir(cal);
	util_git_init();
}

/**
 * Performs sync
 */
void cal_action_sync() {
	cal_git_init();
	util_git_sync();
}

/**
 * Sets repository url
 */
void cal_action_seturl(char *url) {
	cal_git_init();
	util_git_set(url);
}

/**
 * Creates new commit
 */
void cal_commit(char * message) {
	cal_git_init();
	util_git_commit(message);
}

/**
 * Initializes an event list
 */
void event_list_init(event_list *list) {
	list->count = 0;
	list->head = list->tail = NULL;
	list->mp = mp_new(sizeof(event_list_node) * 42);
}

/**
 * Deletes list item
 */
void event_list_free(event_list *list) {
	mp_delete(list->mp);
}

/**
 * Removes an item from the list.
 * Does not free node from memory. Intentionally.
 */
void event_list_remove(event_list *list, event_list_node *node)
{
	if (node->prev != NULL) {
		node->prev->next = node->next;
		if (node->next != NULL)
			node->next->prev = node->prev;
		else list->tail = node->prev;
	} else {
		list->head = node->next;
		if (node->next != NULL)
			node->next->prev = NULL;
	}
	node->prev = node->next = NULL;
	list->count--;
}

/**
 * Appends existing list item to list
 */
void event_list_append(event_list *list, event_list_node *node)
{
	node->next = NULL;
	if (list->tail != NULL) {
		list->tail->next = node;
		node->prev = list->tail;
	}
	list->tail = node;
	if (list->head == NULL)
		list->head = node;
	list->count++;
}

/**
 * Appands new list item to list
 */
event_list_node *event_list_new(event_list *list)
{
	event_list_node *node = mp_alloc_zero(list->mp, sizeof(event_list_node));
	event_list_append(list, node);
	return node;
}

/**
 * Moves head from list to another list
 */
void event_list_move_head(event_list *from, event_list *to)
{
	event_list_node *node;
	node = from->head;
	event_list_remove(from, node);
	event_list_append(to, node);
}

/**
 * Merge sort an event_list
 */
void event_list_sort_by_date(event_list *list)
{
	if (list->count <= 1)
		return;

	event_list a, b;
	a.head = a.tail = b.head = b.tail = NULL;
	a.count = b.count = 0;
	while (list->count >= 2) {
		event_list_move_head(list, &a);
		event_list_move_head(list, &b);
	}
	if (list->count) {
		event_list_move_head(list, &a);
	}

	event_list_sort_by_date(&a);
	event_list_sort_by_date(&b);
	
	while (a.count > 0 && b.count > 0) {
		if (a.head->e.start > b.head->e.start)
			event_list_move_head(&a, list);
		else event_list_move_head(&b, list);
	}
	while (a.count > 0)
		event_list_move_head(&a, list);
	while (b.count > 0)
		event_list_move_head(&b, list);
}
