#ifndef CAL_H
#define CAL_H

#include "yac.h"
#include "event.h"

typedef struct event_list_s {
	event e;
	struct event_list_s *next, *prev;
} event_list_node;

typedef struct event_list {
	event_list_node *head, *tail;
	size_t count;
	struct mempool *mp;
} event_list;

size_t cal_path(char * dest);
void cal_init();
void cal_mksubdir(char * name);
void cal_git_init();

static event_list_node *_event_list_walker;

/**
 * Behaves like foreach on event list
 */
#define event_list_walk(list, event) \
	for (_event_list_walker = (list)->head; \
		(_event_list_walker != NULL) && (event = &_event_list_walker->e); \
		_event_list_walker = _event_list_walker->next)

#endif
